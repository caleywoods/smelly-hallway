from django.http import HttpResponse
from django.shortcuts import render
from django.views.generic import TemplateView

# Create your views here.
class HomeView(TemplateView):
    template_name = "home.html"
    def get_context_data(self, *args, **kwargs):
        context = super(HomeView, self).get_context_data(*args, **kwargs)
        test_var = "Hey it works!"
        users = [
            {'username': 'caley', 'nickname': '2trill'},
            {'username': 'preston', 'nickname': 'infinite'},
            {'username': 'brian', 'nickname': 'bulldog'}
        ]
        context = {
            'test_var': test_var,
            'users': users
        }

        return context
